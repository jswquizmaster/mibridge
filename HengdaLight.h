/*
Copyright(c) 2022 Johannes Schulte-Wuelwer

Distributed under GPL-3.0 license
*/

#ifndef HENGDALIGHT_H_
#define HENGDALIGHT_H_

#include "SmartLight.h"

class HengdaLight: public SmartLight  {
    protected:
        const uint16_t mSyncwords[2] = {0x050A, 0x55AA};
        const std::vector<uint8_t> mChannels = {0x04, 0x4A};
        const int mBulbType = 0;
        uint8_t mSequenceNumber;

    public:
        HengdaLight(AbstractPL1167 *pl1167);
        void switchON(uint16_t remoteID, uint8_t groupID);
        void switchOFF(uint16_t remoteID, uint8_t groupID);
        void setBrightness(uint16_t remoteID, uint8_t groupID, int brightness);
        void setColor(uint16_t remoteID, uint8_t groupID, int hue);
        void setColorTemperature(uint16_t remoteID, uint8_t groupID, int colorTemp);
};

#endif /* HENGDALIGHT_H_ */
